# 동행 전남지역

## 순천
:notebook:[Description](https://namu.wiki/w/%EC%88%9C%EC%B2%9C%EC%8B%9C)<br>
<a href="https://namu.wiki/w/%EC%88%9C%EC%B2%9C%EC%8B%9C" rel=""><img src="https://gitlab.com/eternalkingdom/accompany_jn/raw/master/imgs/sc.jpg" width= "200px" height="200px;"/><br>
:earth_asia:[Map](https://www.google.co.kr/maps/place/%EC%A0%84%EB%9D%BC%EB%82%A8%EB%8F%84+%EC%88%9C%EC%B2%9C%EC%8B%9C/data=!4m2!3m1!1s0x356e0387a828242d:0x1d0e5aeed8449713?sa=X&ved=0ahUKEwjl-5GlxJXWAhVEk5QKHUKqCXgQ8gEIigEwDw)<br>
<a href="https://www.google.co.kr/maps/place/%EC%A0%84%EB%9D%BC%EB%82%A8%EB%8F%84+%EC%88%9C%EC%B2%9C%EC%8B%9C/@34.962995,127.4698805,13z/data=!3m1!4b1!4m5!3m4!1s0x356e0387a828242d:0x1d0e5aeed8449713!8m2!3d34.950637!4d127.4872135" rel=""><img src="https://gitlab.com/eternalkingdom/accompany_jn/raw/master/imgs/sc_map.PNG" width= "200px" height="200px;"/><br>

### 순천 중앙교회
:notebook:[Description](https://gitlab.com/eternalkingdom/accompany_jn/blob/master/dsc/scjungang/README.md)<br>
<a href="https://gitlab.com/eternalkingdom/accompany_jn/blob/master/dsc/scjungang/README.md" rel=""><img src="https://gitlab.com/eternalkingdom/accompany_jn/raw/master/imgs/scjungang.jpg" width= "200px" height="200px;"/><br>
:earth_asia:[Map](https://www.google.co.kr/maps/place/%EC%88%9C%EC%B2%9C%EC%A4%91%EC%95%99%EA%B5%90%ED%9A%8C/@34.956858,127.4821579,15z/data=!4m5!3m4!1s0x0:0x34c52140ec9b828!8m2!3d34.956858!4d127.4821579)<br>
<a href="https://www.google.co.kr/maps/place/%EC%88%9C%EC%B2%9C%EC%A4%91%EC%95%99%EA%B5%90%ED%9A%8C/@34.956858,127.4821579,15z/data=!4m5!3m4!1s0x0:0x34c52140ec9b828!8m2!3d34.956858!4d127.4821579" rel=""><img src="https://gitlab.com/eternalkingdom/accompany_jn/raw/master/imgs/scjungang_map.PNG" width= "200px" height="200px;"/><br>

## 순천 매산학교
:notebook:[Description](https://gitlab.com/eternalkingdom/accompany_jn/blob/master/dsc/maesan/README.md)<br>
<a href="https://gitlab.com/eternalkingdom/accompany_jn/blob/master/dsc/maesan/README.md"><img src="https://gitlab.com/eternalkingdom/accompany_jn/raw/master/imgs/maesan_present.jpg" width= "200px" height="200px;"/><br>
:earth_asia:[Map](https://www.google.co.kr/maps/search/%EC%A0%84%EB%9D%BC%EB%82%A8%EB%8F%84+%EC%88%9C%EC%B2%9C%EB%A7%A4%EC%82%B0%EC%97%AC%EA%B3%A0/@34.9586121,127.4782471,17z/data=!3m1!4b1)<br>
<a href="https://www.google.co.kr/maps/search/%EC%A0%84%EB%9D%BC%EB%82%A8%EB%8F%84+%EC%88%9C%EC%B2%9C%EB%A7%A4%EC%82%B0%EC%97%AC%EA%B3%A0/@34.9586121,127.4782471,17z/data=!3m1!4b1" rel=""><img src="https://gitlab.com/eternalkingdom/accompany_jn/raw/master/imgs/maesan_map.PNG" width= "200px" height="200px;"/><br>

## 순천 기독교 박물관
:notebook:[Description](https://gitlab.com/eternalkingdom/accompany_jn/blob/master/dsc/scmuseum/README.md)<br>
<a href="https://gitlab.com/eternalkingdom/accompany_jn/blob/master/dsc/scmuseum/README.md"><img src="https://gitlab.com/eternalkingdom/accompany_jn/raw/master/imgs/scmuseum.jpg" width= "200px" height="200px;"/><br>
:earth_asia:[Map](https://www.google.co.kr/maps/place/%EC%88%9C%EC%B2%9C%EA%B8%B0%EB%8F%85%EA%B5%90%EC%97%AD%EC%82%AC%EB%B0%95%EB%AC%BC%EA%B4%80/@34.9602961,127.477932,17z/data=!3m1!4b1!4m5!3m4!1s0x356e0f6817dc2411:0xefc4cb7d9910c54c!8m2!3d34.9602917!4d127.4801207)<br>
<a href="https://www.google.co.kr/maps/place/%EC%88%9C%EC%B2%9C%EA%B8%B0%EB%8F%85%EA%B5%90%EC%97%AD%EC%82%AC%EB%B0%95%EB%AC%BC%EA%B4%80/@34.9602961,127.477932,17z/data=!3m1!4b1!4m5!3m4!1s0x356e0f6817dc2411:0xefc4cb7d9910c54c!8m2!3d34.9602917!4d127.4801207"><img src="https://gitlab.com/eternalkingdom/accompany_jn/raw/master/imgs/scmuseum_map.PNG" width= "200px" height="200px;"/><br>

## 우학리 교회
:notebook:[Description](https://gitlab.com/eternalkingdom/accompany_jn/blob/master/dsc/uhr/README.md)<br>
<a href="https://gitlab.com/eternalkingdom/accompany_jn/blob/master/dsc/uhr/README.md"><img src="https://gitlab.com/eternalkingdom/accompany_jn/raw/master/dsc/uhr/imgs/uhr.jpg" width= "200px" height="200px;"/><br>
:earth_asia:[Map](https://www.google.co.kr/maps/place/%EC%9A%B0%ED%95%99%EB%A6%AC%EA%B5%90%ED%9A%8C+%EB%AA%A9%EC%82%AC%EA%B4%80/@34.5141668,127.7658302,17z/data=!3m1!4b1!4m5!3m4!1s0x356dc65faee809a7:0x72d9aac28b3997fc!8m2!3d34.5141624!4d127.7680189)<br>
<a href="https://www.google.co.kr/maps/place/%EC%9A%B0%ED%95%99%EB%A6%AC%EA%B5%90%ED%9A%8C+%EB%AA%A9%EC%82%AC%EA%B4%80/@34.5141668,127.7658302,17z/data=!3m1!4b1!4m5!3m4!1s0x356dc65faee809a7:0x72d9aac28b3997fc!8m2!3d34.5141624!4d127.7680189"><img src="https://gitlab.com/eternalkingdom/accompany_jn/raw/master/imgs/uhr_map.PNG" width= "200px" height="200px;"/><br>

## 애양원
:notebook:[Description](https://gitlab.com/eternalkingdom/accompany_jn/blob/master/dsc/eyw/README.md)<br>
<a href="https://gitlab.com/eternalkingdom/accompany_jn/blob/master/dsc/eyw/README.md"><img src="https://gitlab.com/eternalkingdom/accompany_jn/raw/master/imgs/eyw.jpg" width= "200px" height="200px;"/><br>
:earth_asia:[Map](https://www.google.co.kr/search?safe=strict&sa=X&rlz=1C1NHXL_koKR730KR730&biw=2560&bih=925&q=%EC%95%A0%EC%96%91%EC%9B%90&npsic=0&rflfq=1&rlha=0&rllag=34841180,127627425,241&tbm=lcl&ved=0ahUKEwiC05Ooo7vWAhWMppQKHVqdAGgQtgMIJw&tbs=lrf:!2m1!1e2!3sIAE,lf:1,lf_ui:2&rldoc=1#rlfi=hd:;si:;mv:!1m3!1d3103.985385960426!2d127.63073804999999!3d34.84280405!2m3!1f0!2f0!3f0!3m2!1i288!2i182!4f13.1;tbs:lrf:!2m1!1e2!3sIAE,lf:1,lf_ui:2)<br>
<a href="https://www.google.co.kr/search?safe=strict&sa=X&rlz=1C1NHXL_koKR730KR730&biw=2560&bih=925&q=%EC%95%A0%EC%96%91%EC%9B%90&npsic=0&rflfq=1&rlha=0&rllag=34841180,127627425,241&tbm=lcl&ved=0ahUKEwiC05Ooo7vWAhWMppQKHVqdAGgQtgMIJw&tbs=lrf:!2m1!1e2!3sIAE,lf:1,lf_ui:2&rldoc=1#rlfi=hd:;si:;mv:!1m3!1d3103.985385960426!2d127.63073804999999!3d34.84280405!2m3!1f0!2f0!3f0!3m2!1i288!2i182!4f13.1;tbs:lrf:!2m1!1e2!3sIAE,lf:1,lf_ui:2"><img src="https://gitlab.com/eternalkingdom/accompany_jn/raw/master/imgs/eyw_map.PNG" width= "200px" height="200px;"/><br>
